<h2>Git guidelines</h2>

<h3>Patches to the original source code</h3>

Debian patches to the package original source must be stored in debian/patches
directory. Debian patches are applied when the package is built and unapplied
when the package is cleaned. The package uses
<a href="packages.debian.org/quilt">quilt</a> <a href="#footnote1">[1]</a>
for managing those patches. Various tasks of patch management (adding, removing
etc.) are described in /usr/share/doc/quilt/README.source (provided by quilt
package of version 0.46-4.1 or later).

<h3>Public VCS repository of Debian packaging</h3>

Debian packaging history of this package is stored in Git
<a href="footnote2">[2]</a> repository.
Repository policy and packaging workflow are described below. This information
is mostly for Debian package maintainers or people following packaging process.
If you are merely preparing a NMU which changes upstream code, feel free to
follow a standard patch creation procedure with quilt as described in the
section above.

<h3>Pushing permissions and occasional contributions</h3>

<ul>
  <li> People in debian/control Maintainer and Uploaders (co-maintainers) fields may
  freely push changes to public repository.</li>
  <li> Other members of packaging group should request permission of the current
  co-maintainers (via IRC or mailing list) before adding yourself to the list of
  Uploaders or pushing any other kind of changes.</li>
  <li> Occasional contributors may create merge requests at
  <a href="https://salsa.debian.org/qt-kde-team" alt="Qt-KDE's salsa project">salsa</a>
  (preferred) or optionally send patches. This last option should preferably be
  formatted with `git format-patch` in order to retain commit authorship
  information.</li>
  <li> Pushed commits and patches must not violate repository policy as defined
  below.</li>
</ul>

<h3>Committing policy and guidelines</h3>

<ul>
  <li> Work-in-progress debian/changelog entry must have its distribution field set to
  UNRELEASED. That's DEBCHANGE_RELEASE_HEURISTIC=changelog setting for dch.</li>
  <li> New packaging changes must be committed together with the change description
  in the debian/changelog entry (if applicable). Sentences should end with dots.</li>
  <li> The guidelines below are *highly* recommended to follow in order to reduce
  conflicts in debian/changelog when cherry-picking and merging between
  branches:
  <ul>
    <li>
    Commits should be self-contained (i.e. one change - one commit).
    </li>
    <li>
    It is a good idea to open a new revision with an empty changelog entry (two
    empty lines between entry header and trailer) as its own commit. When
    packaging a new upstream release, such first commit should include a new
    changelog entry with "New upstream release." as the only change.
    </li>
    <li>
    dch should be configured with DEBCHANGE_MULTIMAINT_MERGE=yes or called with
    --mainttrailer/-t option in order to reduce maintainer trailer changes.
    As a result, dch --release should be used to update the date in the trailer
    when releasing the package to Debian.
    </li>
  </ul></li>
  <li>
  Commit message must be meaningful (typically, debcommit is OK). The first
  commit message line must be short (up to 72-80 characters) and end with a
  dot. More detailed description might be provided in paragraphs wrapped at 80
  characters after a empty line (e.g. like a well formatted standard Git commit
  message).
  </li>
</ul>

See our <a href="changelogstandard.html">standard style for changelog entries</a> for more information.

<h3>Branching policy</h3>


Debian packaging branches must not contain original source and must not have
been merged with a branch containing original source code (referred by name
"upstream" in the rest of this document) in any point of their history. If
necessary, it is recommended to keep a private upstream branch in the private
local repository or just track upstream remote Git repository locally.

Debian packaging branches must only contain debian/ subdirectory in their root
directory in order to avoid potential conflicts with upstream source. This
restriction applies even to Git-specific files like .gitignore.

<h4>Branches should be named as follows:</h4>

<ul>
  <li>
  master - main packaging branch. It typically contains packaging aimed at
  unstable. However, if unstable is (semi-) frozen, it may even contain
  packaging for experimental. It is up package maintainers what is considered
  "current" release.
  </li>
  <li>
  Other mainline branches must be named after Debian release codenames (e.g.
  lenny, squeeze, sid, experimental etc.) that they contain packaging of.
  When such a branch (e.g. experimental) is merged to master, it does not
  need to be deleted from the remote repository.
  </li>
  <li>
  Other packaging "work" branches may be pushed to the remote repository at
  discretion of package maintainer(s). These branches should be deleted from
  the public repository when they are no longer needed.
  </li>
</ul>

Non-fast-forwarding pushes (e.g. rebase of already published history) are
forbidden to all mainline packaging branches unless they are absolutely
necessary and have previously been agreed upon by co-maintainers of the
package.

<h3>Tagging policy</h3>

Tag namespace "debian" is reserved for tagging package releases to Debian.
Preferably, public repository should not contain any other tags unless
otherwise agreed upon.

Each official package release must be tagged. The tag must meet the following
requirements:

<ul>
  <li>
  The tag must mark the state of packaging as uploaded to Debian archive.
  Re-tagging is allowed if necessary.
  </li>
  <li>
  The tag must be annotated and preferably signed with the key of uploader.
  </li>
  <li>
  The tag must be named like "debian/&lt;version&gt;" where &lt;version&gt; is a full
  debian package version without epoch. All occurrences of the ~ character in
  &lt;version&gt; should be replaced with the - character because Git does not
  support ~ character in tag names.
  </li>
  <li>
  The tag must be assigned the message with content like
  "&lt;version&gt;/&lt;distribution&gt;" where &lt;version&gt; is a full debian version of the
  package (without any modifications including epoch) and &lt;distribution&gt;
  is the distribution this package is uploaded to (as per the latest debian
  changelog entry).
  </li>
</ul>

For example, in order to tag the tip of current packaging branch as upload of
the package version 4:4.6.0~beta1-1 to experimental, the following command may
be used:

<br /><br />
<pre>
  $ git tag debian/4.6.0-beta1-1 -sm "4:4.6.0~beta1-1/experimental"
</pre>

<h3>Versioning of upstream snapshots</h3>

Since Git commit IDs are not sequential, it is recommended to use a sequential
number (number of commits since the latest tag) that `git describe` returns at
the tip of the upstream snapshot being packaged.

For example, if:
<br /><br />
<pre>
  $ git describe
  v1.2-beta1-45-67890abc
</pre>
then Debian package upstream version part of this snapshot could be
1.2~beta1+git45+67890abc.

<h3>Cloning public packaging repository</h3>

    <h4>For users with ssh access to alioth:</h4>
    <pre> $ git clone git.debian.org:/git/path/to/the/remote/repository.git</pre>

    <h4>For anonymous users:</h4>
    <pre>$ git clone &lt;Vcs-Git URL from debian/control&gt;</pre>

  Wait a bit. You will end up with full clone of the public repository and a
  local branch master will be setup to track remote master branch
  (origin/master). Change to the directory of the repository.

  In order to track another packaging branch (e.g. lenny) locally, run:

    <br /><br />
    <pre>$ git checkout -b lenny origin/lenny</pre>

  Now let's setup `git push` to push all packaging branches and debian tags by
  default:

    <br /><br />
    <pre>
    $ git config --add remote.origin.push "refs/heads/master"
    $ git config --add remote.origin.push "refs/heads/lenny"
    $ git config --add remote.origin.push "refs/tags/debian/*"
    </pre>

<h3>Pulling changes</h3>

In order to avoid pointless merge commits, it is recommended to use --rebase
option to `git pull` when pulling changes from the remote repository if your
local master contains not-yet-pushed and rather trivial commits:

  <br /><br />
  <pre>
  $ git checkout master
  $ git pull --rebase
  </pre>

<h3>Integrating original source into local repository</h3>

  Obviously, packaging process requires to have original source next to the
  debian/ directory. Since it is not allowed to ship upstream source in the
  public packaging branches, it needs to be retrieved and managed outside
  packaging branches. Basically, there are a couple of options:

<ol>
  <li> Original source is extracted to the packaging branch working tree from
     the tarball:

     <br /><br />
     <pre>
       # (only once) Ignore everything (upstream source) but files under debian/.
       $ git config core.excludesfile debian/upstreamignore

       # Checkout a packaging branch
       $ git checkout master

       # Remove all untracked files including ignored ones. This cleans up the
       # root directory from old upstream sources (if any)
       $ git clean -xdff

       # Extract upstream source code into packaging branch working tree
       $ tar zxvf ../qt4-x11_4.5.2.orig.tar.gz --strip=1

       # Do packaging, committing, finally push...
     </pre>
  </li>
  <li>
     Original source is maintained in the separate (local) branch(es) and is
     read into the packaging branch as needed:
  <ol>
     <li>
         If original source comes in the form of the tarball, it can be
        maintained in the local 'upstream' branch. When creating such a new
        disjoint upstream branch, execute:

        <br /><br />
        <pre>
          # Switch HEAD to not-yet-created upstream branch
          $ git symbolic-ref HEAD refs/heads/upstream
        </pre>

        When updating an existing upstream branch to a new release, just do:

        <br /><br />
        <pre>
          # Clean current branch before checkout
          $ git clean -xdff
          $ git checkout upstream
        </pre>

        Then do something like this:

        <br /><br />
        <pre>
          # Remove all tracked files (e.g. old sources)
          $ git rm -r '*'

          # Extract tarball into the repository
          $ tar zxvf ../qt4-x11_4.5.2.orig.tar.gz --strip=1

          # Add all files to the index and commit the result
          $ git add -A
          $ git commit -m "Import upstream 4.5.2 release."
        </pre>
      </li>
      <li>
        Original source can be fetched from the remote upstream repository.
        For example, let's define a new remote 'qt' for original Qt sources.

        <br /><br />
        <pre>
          $ git remote add qt git://gitorious.org/qt/qt.git
          $ git remote update
          # Wait ...
          $ git branch upstream qt/master # optional
        </pre>
       </li>
    </ol>
     Finally, read upstream tree to the packaging branch working tree, do:

     <br /><br />
     <pre>
       # (only once) Ignore everything (upstream source) but files under
       # debian/.
       $ git config core.excludesfile debian/upstreamignore

       # Make sure there are no dangling untracked files
       $ git clean -xdff

       # Checkout packaging branch
       $ git checkout master

       # Read upstream (can be any git commit'ish/tree'ish reference) sources
       # into the working tree
       $ git read-tree upstream
       $ git checkout-index -a
       $ git reset

       # Do packaging, committing, finally push...
     </pre>
  </li>

  <li>
     Do all packaging work in the private 'build' branch which is a merge of
     upstream (2a or 2b) and packaging branch. This scenario might be quite
     error prone and time consuming since packaging commits must be done in the
     packaging branch rather than build branch or later cherry-picked from the
     build to master branch.

  <ol>
     <li> Committing to master directly:
     <br /><br />
     <pre>
       $ git checkout build # (or git checkout -b build upstream)
       $ git merge upstream # (or git reset --hard upstream)
       $ git merge master

       # Do work
       $ git checkout master
       $ git commit
       $ git checkout build
       $ git merge master

       # Do more work
       $ git checkout master
       $ git commit
       ...
     </pre>
    </li>

    <li> Cherry-picking (or rebasing) from build branch:

     <br /><br />
     <pre>
       $ git checkout build # (or git checkout -b build upstream)
       $ git merge upstream # (or git reset --hard upstream)
       $ git merge master

       # Do work
       $ git commit
       # Do more work
       $ git commit
       ...
       # Done. Now merge packaging changes back to master.

       # Either
       $ git checkout master
       $ git cherry-pick commit1
       $ git cherry-pick commit2
       ...
       $ git branch -D build # build branch becomes useless

       # OR convert build branch into master using rebase
       $ git rebase -i --onto master `git merge-base upstream master`
       $ git checkout master
       $ git reset --hard build

       # Tag, push etc.
      </pre>
     </li>
    </ol>
    </li>
</ol>

  Feel free to use whatever workflow you like most (or any other). In general,
  workflow #3 looks more cumbersome but it's nearer the proper Git way.
  However, workflows #1 are #2 are somewhat simpler even if lower level git
  commands are used. What is more, they give you less headache when managing
  patches with quilt because Git is told to ignore everything but debian/
  subdirectory hence it won't track changes made by `quilt push`. This way you
  can always be sure that `git commit -a` won't pick up any raw patches to
  upstream source regardless if anything is quilt pushed or not.
<br /><br />
[1] Available in the quilt package on Debian based
systems. quilt must be invoked with QUILT_PATCHES environment variables set to
debian/patches.<br />
[2] Available in the git-core package on Debian based
systems.
